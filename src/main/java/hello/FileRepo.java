package hello;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by maxijugermek on 5/13/17.
 */
public interface FileRepo extends CrudRepository<File, Integer> {
    public List<File> findByFile(Integer id);
    public File findBySrc(String path);
}
